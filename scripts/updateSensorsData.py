import serial
import sqlite3

#connect to database
conn = sqlite3.connect('data.db')

c = conn.cursor()

#connect to serial port
ser = serial.Serial(
		port='/dev/ttyACM0',
		baudrate=9600,
		parity=serial.PARITY_NONE,
		stopbits=serial.STOPBITS_ONE,
		bytesize=serial.EIGHTBITS)

# read data from serial
try:
	ser.isOpen()
except Exception, e:
	print "error open serial port: " + str(e)

if ser.isOpen():
    ser.write("t")
    response = ser.readline()
    ser.close()
else:
    print "cannot open serial port"

# insert a new row in logsensors table
c.execute("INSERT INTO logsensors(sensortype,data,timestamp) VALUES ('temp1'," + str(response) +", datetime('now', 'localtime'))")
# c.execute("INSERT INTO logsensors(sensortype, data, timestamp) VALUES ('temp1'," + str(rand) +", datetime('now'))")

# update value for sensortype
c.execute("UPDATE sensors SET value=" + str(response) + ", timestamp=datetime('now', 'localtime') WHERE type='temp1'")

# commit/save changes
conn.commit()

#close conn
conn.close()