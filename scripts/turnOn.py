#!/usr/bin/python

import time

try:
	import RPi.GPIO as GPIO

except RuntimeError:
	print("Error importing RPi.GPIO, please use sudo to achieve this")


#init and define pins
LED = 12

GPIO.setwarnings(False)

GPIO.setmode(GPIO.BOARD)
GPIO.setup(LED,GPIO.OUT)

GPIO.output(LED, 1)

# cleanup
#GPIO.cleanup()

