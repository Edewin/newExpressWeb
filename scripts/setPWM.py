import serial
import sys

pwm = sys.argv[1]

ser = serial.Serial(
		port='/dev/ttyACM0',
		baudrate=9600,
		parity=serial.PARITY_NONE,
		stopbits=serial.STOPBITS_ONE,
		bytesize=serial.EIGHTBITS)
try:
	ser.isOpen()
except Exception, e:
	print "error open serial port: " + str(e)

if ser.isOpen():
	try:
		ser.write(pwm.decode("hex"))
		print "sent: " + str(pwm)
		ser.close()
	except Exception, e:
		print str(e)
else:
	print "cannon open serial port "
