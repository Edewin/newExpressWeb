module.exports = function(io) {
    var express = require('express');
    var router = express.Router();
    var PythonShell = require('python-shell');

    /* GET users listing. */
    router.get('/', function(req, res, next) {
        res.render('remote0eaa2a449df7bd5285eab8f0d2193f85', { title: 'remote' });
    });

    var buttonState = "";
    var usersOnline = 0;
    io.on('connection', function(socket) {
        PythonShell.run('/scripts/readInput.py', function(err, data) {
            if (data == '1') {
                buttonState = "Load is <b>ON</b>";
            } else {
                buttonState = "Load is <b>OFF</b>";
            }
        });
        socket.emit('users', ++usersOnline);
        socket.broadcast.emit('users', usersOnline);

        socket.emit('news', buttonState);
        socket.broadcast.emit('news', buttonState);

        socket.on('disconnect', function() {
            socket.emit('users', --usersOnline);
            socket.broadcast.emit('users', usersOnline);
        });

        socket.on('buttonOn', function(data) {
            buttonState = data;
            PythonShell.run('/scripts/turnOn.py', function(err) {
                if (err) throw err;
            });

            socket.emit('buttonState', data);
            socket.broadcast.emit('buttonState', data);
        });

        socket.on('buttonOff', function(data) {
            buttonState = data;
            PythonShell.run('/scripts/turnOff.py', function(err) {
                if (err) throw err;
            });

            socket.emit('buttonState', data);
            socket.broadcast.emit('buttonState', data);
        });

    });

    return router;
}