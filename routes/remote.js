module.exports = function(io) {
    var express = require('express');
    var router = express.Router();
    var PythonShell = require('python-shell');

    var sqlite3 = require('sqlite3').verbose();
    var db = new sqlite3.Database('data.db');

    /* GET users listing. */
    router.get('/', function(req, res, next) {
        res.render('remote2dd683e2f283c9d30fed25ac8fe2976c', { title: 'remote', io: io, rows: sensorsData });
    });

    var sensorsData;
    var buttonState = "";
    var usersOnline = 0;
    io.on('connection', function(socket) {
        PythonShell.run('/scripts/readInput.py', function(err, data) {
            if (data == '1') {
                buttonState = "Load is <b>ON</b>";
            } else {
                buttonState = "Load is <b>OFF</b>";
            }
        });
        socket.emit('users', ++usersOnline);
        socket.broadcast.emit('users', usersOnline);

        socket.emit('news', buttonState);
        socket.broadcast.emit('news', buttonState);

        socket.on('disconnect', function() {
            socket.emit('users', --usersOnline);
            socket.broadcast.emit('users', usersOnline);
        });

        socket.on('buttonOn', function(data) {
            buttonState = data;
            PythonShell.run('/scripts/turnOn.py', function(err) {
                if (err) throw err;
            });

            socket.emit('buttonState', data);
            socket.broadcast.emit('buttonState', data);
        });

        socket.on('buttonOff', function(data) {
            buttonState = data;
            PythonShell.run('/scripts/turnOff.py', function(err) {
                if (err) throw err;
            });

            socket.emit('buttonState', data);
            socket.broadcast.emit('buttonState', data);
        });

        socket.on('new pwm', function(pwm) {
            var hexString = (parseInt(pwm)).toString(16);
            if (pwm < 16) {
                hexString = "0" + hexString;
            }
            var options = { args: hexString };
            PythonShell.run('/scripts/setPWM.py', options, function(err, data) {
                // nothing for now
            });
        });
        setInterval(function() {
            // get data from sensors and write them to database
            PythonShell.run('/scripts/updateSensorsData.py', function(err, data) {
                // do nothing
            });

            // read from DB and emit a message to sockets
            db.all("SELECT * FROM sensors WHERE type='temp1'",
                function(err, rows) {
                    sensorsData = rows;
                    // console.log(rows);
                    rows.forEach(function(row) {
                        var bufString =
                            row.type + " : " +
                            row.value + " : " +
                            row.timestamp + "&#10&#13";
                        socket.emit("sensor data", bufString);
                    });
                });
        }, 5000);
    });



    return router;
}