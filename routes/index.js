module.exports = function(io) {
    var express = require('express');
    var router = express.Router();
    var PythonShell = require('python-shell');

    // var sqlite3 = require('sqlite3').verbose();
    // var db = new sqlite3.Database('data.db');

    /* GET home page. */
    router.get('/', function(req, res, next) {
        res.render('index', { title: 'Express' });
    });

    io.on('connection', function(socket) {

        socket.on('loginRequire', function(data) {

            // db.all("SELECT username, role FROM users WHERE username ='" +
            //     data.user + "' OR password='" +
            //     data.pass + "'",
            //     function(err, rows) {
            //         rows.forEach(function(row) {
            //             console.log("from db: " + row.username + " : " + row.role);
            //         });
            //     });

            var dataToSend = data.user + " " + data.pass;
            var options = {
                args: dataToSend
            };

            PythonShell.run('/scripts/getUserAndPass.py', options, function(err, results) {
                console.log(results);
                if (results != 'None') {
                    var role = getRole(results);

                    socket.emit('good to go', role);
                } else {
                    socket.emit('wrong pass');
                }
            });
        });
    });

    function getRole(results) {
        var str = String(results);
        str = str.replace(/\s+/g, '');
        var splitted = str.split(",");
        var role = splitted[1].replace(')', '');

        return role;
    }
    return router;
}