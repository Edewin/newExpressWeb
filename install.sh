# install dependencies 
npm install

# update system
sudo apt-get update
sudo apt-get upgrade

# set localtime zone (Romania)
sudo cp /usr/share/zoneinfo/Europe/Bucharest /etc/localtime

# check if there is sqlite3 installed
sudo apt-get install sqlite3

# check and install python-pip
#sudo apt-get install python-pip

#install pysqlite
#pip install pysqlite